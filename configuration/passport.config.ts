import passport from 'passport';
import * as JwtStrategy from "passport-jwt";
import * as LocalStrategy from "passport-local";
import { User } from "../models/user.model";


/**
 * Configures passport authentication middlewares
 * JWT and LocalStrateggy
 */

export function configure() {
    // JSON WEB TOKEN STRATEGY
    // used for REST authentication
    passport.use(new JwtStrategy.Strategy({
        jwtFromRequest: JwtStrategy.ExtractJwt.fromHeader('authorization'),
        secretOrKey: process.env.JWT_SECRET
    }, async(payload, done) => {
        try {
            // Find the user specified in token
            const user = await User.findById(payload.sub);
            // If doesn't exist handle it
            if(!user) {
                return done(null, false);
            }
            // Otherwise, return the user
            done(null, user);
        } catch(error) {
            done(error, false);
        }
    }));

    // LOCAL STRATEGY
    // Used for authentication with db
    passport.use(new LocalStrategy.Strategy({
        usernameField: 'email',
    }, async(email, password, done) => {
        try {
            // find user given the email
            const user = await User.findOne({email});

            // if not handle it
            if(!user) {
                return done(null, false);
            }
            // check if password is correct
            if(!user.isValidPassword) {
                return done(null, false);
            }
            const isMatch = await user.isValidPassword(password);
            console.log('isMatch', isMatch);
            // if not handle it
            if(!isMatch) {
                return done(null, false);
            }
            // otherwise return the user

            done(null, user);
        } catch(error) {
            done(error, false);
        }
    }));
}

export const localAuthentication = passport.authenticate("local", {session: false});
export const jwtAuthentication = passport.authenticate("jwt", {session: false});