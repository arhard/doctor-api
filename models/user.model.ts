import mongoose, { Document } from 'mongoose';
import bcryptjs from "bcryptjs";
const Schema = mongoose.Schema;

/**
 * User model
 */
export interface IUser extends Document {
    email?: string;
    password?: string;
    roles?: string[];
    isValidPassword?(password: string): boolean;
};

/**
 * User schema
 */
const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/
    },
    password: {
        type: String,
        required: true,
        min: 6
    },
    roles: [String]
});


/**
 * Function called before saving user to db
 * Used for hashing and salting password
 * and providing default role 'patient'
 */
userSchema.pre('save', async function(next) {
    try {
        
        const user: IUser = this;

        // if user didin't change his password then we don't want to re-hash his password
        if (!user.isModified('password')) return next();

        user.roles.push('patient'); // user has patient role at the beginning

        // generate a salt
        const salt = await bcryptjs.genSalt(10);
        // hash
        user.password = await bcryptjs.hash(user.password!, salt);
    } catch(error) {
        next(error);
    }
});

/**
 * checks if user's password is the same as provided one
 * @returns true if password are he same and false if not
 */
userSchema.methods.isValidPassword = async function(newPassword) {
    try {
        return await bcryptjs.compare(newPassword, this.password);
    } catch(error) {
        throw new Error(error);
    }
}

export const User = mongoose.model<IUser>('User', userSchema);