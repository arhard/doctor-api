import mongoose, { Document, mongo } from 'mongoose';
const Schema = mongoose.Schema;

/**
 * Clinic model
 */
export interface IClinic extends Document {
    name?: string;
    country?: string;
    postal_code?: string;
    city?: string;
    street?: string;
    building_number?: string;
}

/**
 * Clinic schema
 */
const clinicSchema = new Schema({
    name: {
        type: String,
        required: [true, "CLINIC_SCHEMA_VALIDATION_NAME_FAILED"]
    },
    country: {
        type: String,
        required: [true, "Nazwa kraju jest wymagana"],
        match: [/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/, "Nazwa kraju powinna zaczynać się z wielkiej litery i nie powinna zawierać innych znaków oprócz liter"],
        max: [100, "Nazwa kraju nie może być dłuższa niż 100 znaków"]
    },
    postal_code: {
        type: String,
        required: [true, "Kod pocztowy jest wymagany"],
        match: [/^[0-9]{2}-[0-9]{3}/, "Kod pocztowy powinien być w formacie XX-XXX, gdzie X to cyfra"],
        max: [6, "Kod pocztowy nie może być dłuższy niż 6 znaków"]
    },
    city: {
        type: String,
        required: [true, "Miasto jest wymagane"],
        match: [/^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/, "Nazwa miasta powinna zaczynać się z wielkiej litery i nie może zawierać innych znaków oprócz liter"],
        max: [100, "Nazwa miasta nie może być dłuższa niż 100 znaków"]
    },
    street: {
        type: String,
        required: [true, "Ulica jest wymagana"],
        match: [/^[A-ZŻŹĆĄŚĘŁÓŃ0-9][\-a-zżźćąśęłóń0-9]*/, "Nazwa ulicy powinna zaczynać się z wielkiej litery, zawierać polskie znaki, cyfry lub myslnik"],
        max: [100, "Nazwa ulicy może mieć maksymalnie 100 znaków"]
    },
    building_number: {
        type: String,
        required: [true, "Numer budynku jest wymagany"],
         match: /\d+[a-zA-Z]?\/?(\d+)?/
    }
});


export const Clinic = mongoose.model<IClinic>('Clinic', clinicSchema);

