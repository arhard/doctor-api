import mongoose, { Document } from 'mongoose';
import { IVisit } from './visit.model';
const Schema = mongoose.Schema;

/**
 * Schedule model
 */
export interface ISchedule extends Document {
    start?:Date;
    end?: Date;
    doctor_id?: string;
    clinic_id?: string;
    visits?: IVisit[];
}


/**
 * Schedule schema
 */
const scheduleSchema = new Schema({
    start:{
        type: Schema.Types.Date,
        required: true
    },
    end: {
        type: Schema.Types.Date,
        required: true
    },
    doctor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Doctor',
        required: true,
    },
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'Clinic',
        required: true
    },
    visits: [{
        type: Schema.Types.ObjectId,
        ref: 'Visit',
        required: true
    }]
});

export const Schedule = mongoose.model<ISchedule>('Schedule', scheduleSchema);