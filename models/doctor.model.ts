import mongoose, { Document } from 'mongoose';
import { ISchedule } from './schedule.model';
const Schema = mongoose.Schema;

/**
 * Doctor model
 */
export interface IDoctor extends Document {
    first_name?: string;
    last_name?: string;
    specialities?: string[];
    schedules?: ISchedule[] | any;
}

/**
 * Doctor schema
 */
export const doctorSchema = new Schema({
    first_name: {
        type: String,
        required: true,
        match: /^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/,
        max: 100
    },
    last_name: {
        type: String,
        required: true,
        match: /^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/,
        max: 100
    },
    specialities: {
        type: [String],
        required: true
    },
    schedules: [{
        type: Schema.Types.ObjectId,
        ref: 'Schedule',
        required: true
    }]
});

export const Doctor = mongoose.model<IDoctor>('Doctor', doctorSchema);