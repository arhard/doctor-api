import mongoose, { Document } from 'mongoose';
import { IUser } from './user.model';
const Schema = mongoose.Schema;


/**
 * Patient model
 */
export interface IPatient extends Document {
    first_name?: string;
    last_name?: string;
    national_number?: string;
    user_id?: any | IUser;
    visits?: any[]
}

/**
 * Patien schema
 */
const patientSchema = new Schema({
    first_name: {
        type: String,
        required: [true, "Imię jest wymagane."],
        match: /^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/,
        max: 100
    },
    last_name: {
        type: String,
        required: [true, "Nazwisko jest wymagane."],
        match: /^[A-ZŻŹĆĄŚĘŁÓŃ][a-zżźćąśęłóń]*/,
        max: 100
    },
    national_number: {
        type: String,
        required: [true, 'Numer PESEL jest wymagany'],
        validate: {
            /**
             * Validates PESEL number according to it's checksum 
             */
            validator: function(v: string) {
                const checkSum 
                    = 9 * parseInt(v.charAt(0))
                    + 7 * parseInt(v.charAt(1))
                    + 3 * parseInt(v.charAt(2))
                    + 1 * parseInt(v.charAt(3))
                    + 9 * parseInt(v.charAt(4))
                    + 7 * parseInt(v.charAt(5))
                    + 3 * parseInt(v.charAt(6))
                    + 1 * parseInt(v.charAt(7))
                    + 9 * parseInt(v.charAt(8))
                    + 7 * parseInt(v.charAt(9));

                const stringifiedCheckSum = checkSum.toString();
                // last digit of a checksum must be equal to last digit of a pesel number
                return (stringifiedCheckSum.charAt(stringifiedCheckSum.length-1) === v.charAt(10));
            },
            message: '{VALUE} nie jest poprawnym numerem PESEL!'
        }
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        unique: true
    },
    visits: [{
        type: Schema.Types.ObjectId,
        ref: 'Visit'
    }]
});


// create a model
export const Patient = mongoose.model<IPatient>('Patient', patientSchema);