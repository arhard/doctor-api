import mongoose, { Document } from 'mongoose';
const Schema = mongoose.Schema;

/**
 * Visit model
 */
export interface IVisit extends Document {
    _id: any;
    start?: Date | any;
    end?: Date | any;
    reason?: string;
    completed?: boolean;
    canceled?: boolean;
    cancelation_reason?: string;
    schedule_id?: string;
    patient_id?: string;

}

/**
 * Visit schema
 */
const visitSchema = new Schema({
    start: {
        type: Schema.Types.Date,
        required: true
    },
    end: {
        type: Schema.Types.Date,
        required: true
    },
    reason: {
        type: String,
        required: true,
        max: 200
    },
    completed: {
        type: Boolean,
        required: true
    },
    canceled: {
        type: Boolean,  
        required: true
    },
    cancelation_reason: {
        type: String,
        max: 200,
    },
    schedule_id: {
        type: Schema.Types.ObjectId,
        ref: 'Schedule',
        required: true,
    },
    patient_id: {
        type: Schema.Types.ObjectId,
        ref: 'Patient',
        required: true
    },
});

export const Visit = mongoose.model<IVisit>('Visit', visitSchema);