import PromiseRouter from "express-promise-router";
import * as PatientController from "../controllers/patient.controller";
import * as auth from '../middlewares/authorization.middleware';
import * as passportConfig from "../configuration/passport.config";


const router = PromiseRouter();
router.use(passportConfig.jwtAuthentication);
// all patients
router.route('/')
    .get(auth.checkForRoles(process.env.ROLE_ADMIN), PatientController.findAll)
    .post(PatientController.save);

// patient by id
router.route('/:id')
    .get(auth.checkOwnerPatient("id"), PatientController.findById)
    .patch(auth.checkForRoles(process.env.ROLE_ADMIN), PatientController.updateById)
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), PatientController.deleteById)

// patient's visit
router.route('/:id/visits')
    .get(auth.checkForRoles(process.env.ROLE_ADMIN), PatientController.findAllVisits)
    .post(PatientController.insertVisit);

router.route('/:id/user/:user_id')
    .all(auth.checkForAnyRole(process.env.ROLE_DOCTOR, process.env.ROLE_ADMIN))
    .post(PatientController.insertUser);

router.route('/:id/visits/:visit_id')
    .delete(PatientController.deleteVisitById);
// patient's user
router.route('/user/:id')
    .all(passportConfig.jwtAuthentication, auth.checkForAnyRole(process.env.ROLE_ADMIN, process.env.ROLE_PATIENT))
    .get(PatientController.findByUserId);

module.exports = router;