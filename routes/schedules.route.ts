import PromiseRouter from "express-promise-router";
import * as passportConfig from "../configuration/passport.config";
import * as ScheduleController from "../controllers/schedule.controller";
import * as auth from '../middlewares/authorization.middleware';


const router = PromiseRouter();
passportConfig.configure();

router.route('/')
    .all(passportConfig.jwtAuthentication)
    .get(ScheduleController.findAll)
    .post(auth.checkForRoles(process.env.ROLE_ADMIN),ScheduleController.save);

router.route('/bydate')
    .all(passportConfig.jwtAuthentication)
    .get(ScheduleController.findBetweenDates);

router.route('/:id')
    .all(passportConfig.jwtAuthentication)
    .get(ScheduleController.findById)
    .patch(auth.checkForRoles(process.env.ROLE_ADMIN), ScheduleController.updateById)
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), ScheduleController.deleteById);

router.route('/:id/visits')
    .all(passportConfig.jwtAuthentication)
    .post(ScheduleController.insertVisit);



module.exports = router;