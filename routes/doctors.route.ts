import PromiseRouter from "express-promise-router";
import passport from "passport";
import * as passportConfig from "../configuration/passport.config";
import * as DoctorController from "../controllers/doctor.controller";
import * as auth from '../middlewares/authorization.middleware';
import { Router } from "../node_modules/@types/express";


const router = PromiseRouter();

router.use(passportConfig.jwtAuthentication)

router.route('/')
    .all(auth.checkForRoles(process.env.ROLE_ADMIN))
    .get(DoctorController.findAll)
    .post(DoctorController.save);

router.route('/:id')
    .get(DoctorController.findById)
    .patch(auth.checkForRoles(process.env.ROLE_ADMIN), DoctorController.updateById)
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), DoctorController.deleteById);


router.route('/:id/schedules')
    .post(auth.checkForRoles(process.env.ROLE_ADMIN), DoctorController.addSchedule);

router.route('/:id/schedules/:schedule_id')
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), DoctorController.deleteSchedule);

module.exports = router;