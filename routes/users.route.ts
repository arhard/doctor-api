import PromiseRouter from "express-promise-router";
import * as passportConfig from "../configuration/passport.config";
import * as UsersController from "../controllers/user.controller";
import * as auth from '../middlewares/authorization.middleware';


const routeHelpers = require("../helpers/route.helper");

const router = PromiseRouter()


router.route('/signup')
    .post(routeHelpers.validateBody(routeHelpers.authSchema), UsersController.signUp);

router.route('/signin')
    .all(passportConfig.localAuthentication)
    .post(routeHelpers.validateBody(routeHelpers.authSchema), UsersController.signIn);

// only administrators can mainpulate on users
router.route('/')
    .all(passportConfig. jwtAuthentication, auth.checkForRoles(process.env.ROLE_ADMIN)) 
    .get(UsersController.findAll);

router.route('/:id')
    .all(passportConfig.jwtAuthentication, auth.checkForRoles(process.env.ROLE_ADMIN))
    .get(UsersController.findById)
    .patch(UsersController.updateById)
    .delete(UsersController.deleteById);

module.exports = router;