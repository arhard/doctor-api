import PromiseRouter from "express-promise-router";

import * as passportConfig from "../configuration/passport.config";
import * as ClinicController from "../controllers/clinic.controller";
import * as auth from "../middlewares/authorization.middleware";


const router = PromiseRouter();

router.route('/')
    .all(passportConfig.jwtAuthentication)
    .get(ClinicController.findAll)
    .post(auth.checkForRoles(process.env.ROLE_ADMIN), ClinicController.save);

router.route('/:id')
    .all(passportConfig.jwtAuthentication)
    .get(ClinicController.findById)
    .patch(auth.checkForRoles(process.env.ROLE_ADMIN), ClinicController.updateById)
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), ClinicController.deleteById);

module.exports = router;