import PromiseRouter from "express-promise-router";
import * as passportConfig from "../configuration/passport.config";
import * as VisitController from "../controllers/visit.controller";
import * as auth from '../middlewares/authorization.middleware';


const router = PromiseRouter();


router.route('/')
    .all(passportConfig.jwtAuthentication)
    .get(auth.checkForRoles(process.env.ROLE_ADMIN), VisitController.findAll)
    .post(VisitController.save);

router.route('/byschedule/:schedule_id')
    .all(passportConfig.jwtAuthentication)
    .get(VisitController.findByScheduleId);

router.route('/:id')
    .all(passportConfig.jwtAuthentication)
    .get(VisitController.findById)
    .patch(VisitController.updateById)
    .delete(auth.checkForRoles(process.env.ROLE_ADMIN), VisitController.deleteById);

module.exports = router;