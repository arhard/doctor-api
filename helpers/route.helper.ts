import * as Joi from "joi";
import { Request, Response, NextFunction } from "express";

/**
 * Middleware that validates request body according to provieded schema
 * @param schema schema used for validation
 * @returns 400 response in case of error
 */
export function validateBody(schema) {
    return (req: any, res: Response, next: NextFunction) => {
        const result = Joi.validate(req.body, schema);
        if(result.error) {
            return res.status(400).json(result.error);
        }
        if(!req.value) {
            req.value = {};
        }

        req.value['body'] = result.value;
        next();
    }
}

/**
 * Schema used for validating user auth body
 */
export const authSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
});
