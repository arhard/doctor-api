import {Request, Response, NextFunction} from 'express';
import { PatientRepository } from "../repositories/patient.repository";
import { VisitRepository } from "../repositories/visit.repository";
import { IVisit } from '../models/visit.model';


const patientRepository = PatientRepository.getInstance();
const visitRepository   = VisitRepository.getInstance();


export async function save(req: Request, res: Response, next: NextFunction) {
    const patient = await patientRepository.save(req.body);
    res.status(201).json(patient);
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    const result = await patientRepository.findById(req.params.id);
    res.status(200).json(result);
}

export async function findAll(req: Request, res: Response, next: NextFunction) {
    const result = await patientRepository.findAll();
    res.status(200).json(result);
}
export async function updateById(req: Request, res: Response, next: NextFunction) {
    const result = await patientRepository.update(req.body);
    res.status(200).json(result);
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    await patientRepository.deleteById(req.params.id);
    res.status(204).json({message: "ok"});
}

export async function insertVisit(req: Request, res: Response, next: NextFunction) {
    
    try {
        // get visit
        const visit = req.body

        // get patient's all visits
        const patient = await patientRepository.findById(req.params.id);
        const visits = patient.visits;

        // check if he doesn't have a visit at that date
        const overlapingVisitsCount = visits.filter((v: IVisit) => v.start === visit.start).length;

        // if he has
        if(overlapingVisitsCount > 0) {
            throw new Error('Masz już wizytę w tym terminie');
        }
        
        // if no then add it
        patientRepository.addVisitId(req.params.id, visit._id);
        res.status(201).json(visit);
    } catch (error) {
        console.log(error);
        return res.status(400).json(error);
    }
    
}

export async function findAllVisits(req: Request, res: Response, next: NextFunction) {
    const patient = await patientRepository.findById(req.params.id);
    res.status(200).json(patient.visits);
}

export async function deleteVisitById(req: Request, res: Response, next: NextFunction) {
    // delete visit
    visitRepository.deleteById(req.params.visit_id);

    // delete visit's id at patient
    patientRepository.removeVisitId(req.params.id, req.params.visit_id);
}

export async function findByUserId(req: Request, res: Response, next: NextFunction) {
    const patient = await patientRepository.findByUserId(req.params.id);
    res.status(200).json(patient);
}

export async function insertUser(req: Request, res: Response, next: NextFunction) {
    res.status(201).json(await patientRepository.insertUser(req.params.id, req.params.user_id));
}