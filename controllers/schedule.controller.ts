import {Request, Response, NextFunction} from 'express';

import { ScheduleRepository } from "../repositories/schedule.repository";
import moment from "moment";
import { ISchedule, Schedule } from '../models/schedule.model';



const scheduleRepository = ScheduleRepository.getInstance();

export async function save(req: Request, res: Response, next: NextFunction) {
    if(!await scheduleOverlapsWithExistings(req.body)) {
        const schedule = await scheduleRepository.save(req.body);
        res.status(201).json(schedule);
    } else { // else bad request
        res.status(400).json("W podanym zakresie dat zdefiniowano już grafik dla tego lekarza");
    }
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await scheduleRepository.findById(req.params.id));
}
export async function findBetweenDates(req: Request, res: Response, next: NextFunction) {
    // if query params for date are missing
    if(!req.query.start || !req.query.end) {
        res.status(400).json({message: "Missing query parameters."});
    } else {
        res.status(200).json(await scheduleRepository.findBetweenDates(new Date(req.query.start), new Date(req.query.end)));
    }
}
export async function findAll(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await scheduleRepository.findAll());
}

export async function updateById(req: Request, res: Response, next: NextFunction) {
    // while updating schedule can overlap with itself
    if(!await scheduleOverlapsWithExistings(req.body, true)) {
        res.status(200).json(await scheduleRepository.update(req.body));
    } else { // else bad request
        res.status(400).json("W podanym zakresie dat zdefiniowano już grafik dla tego lekarza");
    }
    
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    await scheduleRepository.deleteById(req.params.id);
    res.status(204).json({message: "ok"});
}

export async function insertVisit(req: Request, res: Response, next: NextFunction) {
    try {
        res.status(200).json(await scheduleRepository.insertVisit(req.params.id, req.body));
    } catch(error) {
        console.log(error);
        res.status(400).json(error);
    }
}

async function scheduleOverlapsWithExistings(schedule: ISchedule, allowForSelfOverlaping = false) {
    const overlapingSchedules = (await scheduleRepository
        .findBetweenDatesAndDoctorId(schedule.start, schedule.end, schedule.doctor_id))
        .filter(s => moment(schedule.start).isSameOrBefore(s.end) && moment(schedule.end).isSameOrAfter(s.start))

    if(overlapingSchedules.length === 1 && allowForSelfOverlaping === true) {
        if(schedule._id.toString() === overlapingSchedules[0]._id.toString()) {
            return false;
        } else {
            return true;
        }
    } else {
        return overlapingSchedules.length > 0;
    }
}