import { Request, Response, NextFunction } from 'express';
import { DoctorRepository } from "../repositories/doctor.repository";


const doctorRepository = DoctorRepository.getInstance();



export async function save(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.save(req.body);
    res.status(201).json(doctor);
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.findById(req.params.id);
    res.status(200).json(doctor);
}

export async function findAll(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.findAll();
    res.status(200).json(doctor);
}
export async function updateById(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.update(req.body);
    res.status(200).json(doctor);
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    await doctorRepository.deleteById(req.params.id);
    res.status(204).json({message: "ok"});
}

export async function addSchedule(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.addSchedule(req.params.id, req.body._id);
    res.status(200).json(doctor);
}

export async function deleteSchedule(req: Request, res: Response, next: NextFunction) {
    const doctor = await doctorRepository.deleteSchedule(req.params.id, req.params.schedule_id);
    res.status(200).json(doctor);
}