import {Request, Response, NextFunction} from 'express';
import { ClinicRepository } from '../repositories/clinic.repository';


const clinicRepository = ClinicRepository.getInstance();

export async function save(req: Request, res: Response, next: NextFunction) {
    const clinic = await clinicRepository.save(req.body);
    res.status(201).json(clinic);
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await clinicRepository.findById(req.params.id));
}

export async function findAll(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await clinicRepository.findAll());

}

export async function updateById(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await clinicRepository.update(req.body));
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    await clinicRepository.deleteById(req.params.id);
    res.status(204).json({message: "ok"});
}