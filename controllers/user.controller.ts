import {Request, Response, NextFunction} from 'express';
import { UserRepository } from "../repositories/user.repository";
import * as JWT from "jsonwebtoken";

const userRepository = UserRepository.getInstance();

function signToken(user) {
    const foundUser = userRepository.findByEmail(user.email);

    const token = JWT.sign({
        iss: 'Maciej Dados',
        sub: user._id,
        iat: new Date().getTime(), // current time
        exp: new Date().setDate(new Date().getDate() + 1) // one day ahead 
    }, <string>(process.env.JWT_SECRET));
    return token;
}

export async function save(req: Request, res: Response, next: NextFunction) {
    const result = await userRepository.save(req.body);
    res.status(201).json(result);
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    await userRepository.deleteById(req.params.id);
    res.status(204).json({message: "deleted"});
}

export async function findAll(req: Request, res: Response, next: NextFunction) {
    const result = await userRepository.findAll();
    res.status(200).json(result);
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    const result = await userRepository.findById(req.params.id);
    res.status(200).json(result);
}

export async function updateById(req: Request, res: Response, next: NextFunction) {
    const user = await userRepository.update(req.body);
    
    res.status(200).json(user);
}

// registers user in the system, by default new user is always a patient
export async function signUp(req: Request | any, res: Response, next: NextFunction) {   
    const {email, password } = req.value.body;

    const foundUser = await userRepository.findByEmail(email); 
    console.log('foundUser', foundUser);
    if(foundUser) {
        res.status(403).json('Email is already in use');
        return;
    }

    const newUser = userRepository.save(req.value.body);


    const token = signToken(newUser);
    res.status(200).json({token});  

}

export async function signIn(req: Request, res: Response, next: NextFunction) {
    res.json({id: req.user.id, token: signToken(req.user), roles: req.user.roles, email: req.user.email});
}
