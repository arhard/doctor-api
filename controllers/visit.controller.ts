import {Request, Response, NextFunction} from 'express';

import { VisitRepository } from "../repositories/visit.repository";
import { PatientRepository } from "../repositories/patient.repository";
import { ScheduleRepository } from "../repositories/schedule.repository";


const patientRepository     = PatientRepository.getInstance();
const scheduleRepository    = ScheduleRepository.getInstance();
const visitRepository       = VisitRepository.getInstance();

export async function save(req: Request, res: Response, next: NextFunction) {
    const visit = await visitRepository.save(req.body);
    res.status(201).json(visit);
}

export async function findById(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await visitRepository.findById(req.params.id));
}
export async function findByScheduleId(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await visitRepository.findByScheduleId(req.params.schedule_id));
}

export async function findAll(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await visitRepository.findAll());

}

export async function updateById(req: Request, res: Response, next: NextFunction) {
    res.status(200).json(await visitRepository.update(req.body));
}

export async function deleteById(req: Request, res: Response, next: NextFunction) {
    // get visit
    const visit = await visitRepository.findById(req.params.id);

    // delete visit
    await visitRepository.deleteById(req.params.id);

    // delete visit from patient
    await patientRepository.removeVisitId(visit.patient_id, visit._id);

    // delete visit from schedule
    await scheduleRepository.removeVisit(visit.schedule_id, req.params.id);
    
    res.status(204).json({message: "ok"});
}