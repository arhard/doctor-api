// config
import dotenv from "dotenv";
dotenv.config();

// libs
import express from "express";
import mongoose from "mongoose";
import morgan from "morgan"
import bodyParser from "body-parser"
import cors from "cors";
import colors from "colors/safe";
import * as fs from "fs-extra";
import * as passport from './configuration/passport.config';

// db
mongoose.connect(process.env.DB_URL)
    .then(() => {
        console.log(colors.yellow(`${new Date()}\tConnected to db`));
        main();
    })
    .catch(() => {
        console.log(colors.red('Couldn\'t connect to db!'));
    });

async function loadRoutes(app, directory: string, prefix: string) {
    (await fs.readdir(directory))
        .forEach(name => {
            console.log(colors.green(`Added route /${prefix}/${name.split('.')[0]}`));
            app.use(`/${prefix}/${name.split('.')[0]}`, require(`./${directory}/${name.substr(0, name.length-3)}`));
        });
}

async function main() {
    const app = express();

    // middlewares
    app.use(cors());
    app.use(morgan('dev'));
    app.use(bodyParser.json());


    // auth
    passport.configure();

    // configure routes
    await loadRoutes(app, "routes", "api");

    // error handler
    app.use((err, req: express.Request, res: express.Response, next: express.NextFunction) => {
        console.log(colors.red(`${new Date()}\tERROR HANDLER: ${err}`));
        res.status(400).json(err);
    });
    // boot 
    app.listen(process.env.PORT, () => {
        console.log(colors.yellow(`${new Date()}\tServer started at port ${process.env.PORT}`));
    });
}

