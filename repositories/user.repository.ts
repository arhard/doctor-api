import { User, IUser } from '../models/user.model';
import { AbstractRepository } from './abstract.repository';


/**
 * User crud repository
 */
export class UserRepository extends AbstractRepository<IUser, string> {
    /**
     * Singleton instance
     */
    private static instance = new UserRepository(User);
    
    /**
     * Returns instance
     * @return singleton instance
     */
    static getInstance() {
        return UserRepository.instance;
    }

    /**
     * Returns user with specified id, excluding password
     * @param id user's id
     * @returns user with specified id
     */
    async findById(id: string) {
        return await super.findById(id, "_id email roles");
    }

    /**
     * Returns user with specified email, excluding password
     * @param email user's email
     * @returns user with specified email address
     */
    async findByEmail(email: string) {
        return await super.findOneBy({email: email}, "_id email roles");
    }

}
