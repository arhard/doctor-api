import { IDoctor, Doctor } from '../models/doctor.model';
import { AbstractRepository } from './abstract.repository';


/**
 * Doctor crud repository with additional methods
 */
export class DoctorRepository extends AbstractRepository<IDoctor, string> {
    /**
     * Singleton instance
     */
    private static instance = new DoctorRepository(Doctor, ['schedules']);
    
    /**
     * Returns instance
     * @return singleton instance
     */
    static getInstance() {
        return DoctorRepository.instance;
    }

    /**
     * Inserts schedule id into doctor's schedules
     * @param id doctor's id that will have schedule id inserted
     * @param schedule_id schedule id that will be inserted
     * @returns updated doctor
     */
    async addSchedule(id: string, schedule_id: string) {
        // get
        const doctor = await this.findById(id);

        // insert schedule id
        doctor.schedules.push(schedule_id);
        await doctor.save();
        return doctor;
    }

    /**
     * Removes schedule id from doctor's schedules
     * @param id doctor's id that will have schedule removed
     * @param schedule_id schedule's id that will be removed from doctor
     * @returns updated doctor
     */
    async deleteSchedule(id: string, schedule_id: string) {
        const doctor = await this.findById(id);
        doctor.schedules = doctor.schedules
            .filter(schedule => {schedule._id != schedule_id });
        await doctor.save();
        return doctor;
    }
}
