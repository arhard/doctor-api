import { IClinic, Clinic } from '../models/clinic.model';
import { AbstractRepository } from './abstract.repository';

/**
 * Clinic crud repository
 */
export class ClinicRepository extends AbstractRepository<IClinic, string> {
    /**
     * Singleton instance
     */
    private static instance = new ClinicRepository(Clinic);
    
    /**
     * Returns an instance
     * @return singleton instance
     */
    static getInstance() {
        return ClinicRepository.instance;
    }
}
