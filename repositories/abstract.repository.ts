import { Model, Document, DocumentQuery } from "mongoose";


/**
 * Defines abstract repository with basic crud operations
 * @param V defines document stored in a collection
 * @param K primary key type
 */
export abstract class AbstractRepository<V extends Document, K> {

    /**
     * Constructor
     * @param model Mongoose model used for querying
     * @param populateOptions names of fields that will be populated
     */
    protected constructor(protected model: Model<V>, protected populateOptions?: string[]) { }

    /**
     * Populates query with names of fields containing references to other collections that will be populated
     * @param query Query that will be populated
     * @param fields Define field names, defaulted to populateOptions from constructor
     * @returns populated query
     */
    private populateFields(query: DocumentQuery<any, V>, fields: string[] = this.populateOptions) {
        if(fields) {
            // agregate field names to populate
            this.populateOptions.forEach(option => {
                query = query.populate(option);
            });
        }
        return query;
    }

    /**
     * Saves an element and returns it
     * @param body element to be saved
     * @returns saved object
     */
    async save(body: V) {
        const value = new this.model(body);
        await value.save();
        return value;
    }

    /**
     * Finds and returns all documents from collection
     * @param selectParams mongo object with selected fields that will be returned, defaulted to all
     * @returns all documents from collection
     */
    async findAll(selectParams?: string) {
    
        let query = this.model
            .find({})
            .select(selectParams);
        
        query = this.populateFields(query);

        return await query;
    }

    /**
     * Finds and returns a single document from collection queried by it's id
     * @param id id used for quering
     * @param selectParams mongo object with selected fields that will be returned, defaulted to all
     * @returns found element
     */
    async findById(id: K, selectParams?: string) {
        let query = this.model
            .findById(id)
            .select(selectParams);
        
        query = this.populateFields(query);
        return await query;
    }

    /**
     * Find one element basing on provided query params
     * @param params Query params
     * @param selectParams selected fields
     * @return found element
     */
    async findOneBy(params: Object, selectParams?: string) {
        let query = this.model
            .findOne(params)
            .select(selectParams);

        query = this.populateFields(query);
        return await query;
    }

    /**
     * Finds elemends basing on provided query params
     * @param params Query params
     * @param selectParams selected fields\
     * @returns array of elements
     */
    async findBy(params: Object, selectParams?: string) {
        let query = this.model
            .find(params)
            .select(selectParams);

        query = this.populateFields(query);
        return await query;
    }

    /**
     * Deletes a single element from collection
     * @param id id of element to be deleted
     * @return deleted element
     */
    async deleteById(id: K) {
        const element = await this.model.findById(id);
        await this.model.deleteOne({_id: id});
        return element;
    }

    /**
     * Updates a single element using id from body
     * @param body element that is about to be updated
     * @return updated element
     */
    async update(body: V) {
        console.log('update got body:', body);
        const element = await this.model.findById(body._id);

        for(let key in element) {
            // if there is a field with a key then update, if no then skip
            if(body[key] !== undefined) {
                element[key] = body[key];
            }
        }

        await element.save();
        return element;
    }
}