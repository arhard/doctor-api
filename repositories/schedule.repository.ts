import { Schedule, ISchedule } from '../models/schedule.model';
import { IVisit } from '../models/visit.model';
import { AbstractRepository } from './abstract.repository';


/**
 * Schedule crud repository
 */
export class ScheduleRepository extends AbstractRepository<ISchedule, string> {
    /**
     * Singleton instance
     */
    private static instance = new ScheduleRepository(Schedule, ['doctor_id', 'clinic_id', 'visits']);
    
    /**
     * Returns instance
     * @return singleton instance
     */
    static getInstance() {
        return ScheduleRepository.instance;
    }

    /**
     * Returns schedule of a specified doctor within a time interval
     * @param start start date
     * @param end end date
     * @param doctorId id of doctor whose schedules will be returned
     * @returns schedules
     */
    async findBetweenDatesAndDoctorId(start: Date, end: Date, doctorId: string) {
        start = new Date(start);
        end = new Date(end);

        start.setUTCHours(0, 0); // we want to start with the beginnin of the day
        end.setUTCHours(23, 59); // set to 23:59 because we want to finde schedules including whole day
        return await this.findBy({
            start: {$gte: start},
            end: {$lte: end},
            doctor_id: doctorId
        });
    }

    /**
     * All schedules within specified time interval
     * @param start start date
     * @param end end date
     * @returns schedules
     */
    async findBetweenDates(start: Date, end: Date) {
        start.setHours(0, 0); // we want to start with the beginnin of the day
        end.setHours(23, 59); // set to 23:59 because we want to finde schedules including whole day
        return await this.findBy({
            start: {$gte: start},
            end: {$lte: end}
        });
    }


    /**
     * Inserts visit into schedule
     * @param id schedule's id
     * @param visit visit to be inserted
     * @returns updated schedule
     */
    async insertVisit(id: string, visit: IVisit) {
        const schedule = await this.findById(id);
        schedule.visits.push(visit._id || visit.id);
        await schedule.save();
        return schedule;
    
    }
    

    /**
     * Removes visit from schedule
     * @param id schedule's id
     * @param visit_id visit id to be deleted from schedule
     * @returns updated schedule
     */
    async removeVisit(id: string, visit_id: string) {
        const schedule = await this.findById(id);
        schedule.visits = schedule.visits.filter((visit:any) => visit != visit_id);
        await schedule.save();    
        return schedule;
    }

}
