import { Patient, IPatient } from '../models/patient.model';
import { AbstractRepository } from './abstract.repository';

/**
 * Patient crud repository with additional methods
 */
export class PatientRepository extends AbstractRepository<IPatient, string> {
    
    /**
     * Singleton instance
     */
    private static instance = new PatientRepository(Patient, ['user_id', 'visits']);
    
    /**
     * Returns instance
     * @return singleton instance
     */
    static getInstance() {
        return PatientRepository.instance;
    }

    /**
     * Assigns user to a patient
     * @param id patient's id who will have user assigned
     * @param user_id user's id who will be assigned to patient
     */
    async insertUser(id: any, user_id: any) {
        const patient = await this.findById(id);
        patient.user_id = user_id;
        await patient.save();
        return patient;
    }

    /**
     * Adds visit to a patient
     * @param id patient's id who will have visit added
     * @param visit_id visit's id that will be added to patient's visit array
     * @returns updated patient
     */
    async addVisitId(id: string, visit_id: string) {
        const result = await this.model
            .findById(id);
        
        result.visits.push(visit_id);
        await result.save();
        return result;
    }

    /**
     * Removes visit from patient
     * @param id patient's id who will have visit removed
     * @param visit_id vitit id that will be removed from patient's visit array
     * @returns updated patient
     */
    async removeVisitId(id: any, visit_id: string) {
        const patient = await Patient.findById(id);
        // at this point visits are stored as id's 
        if(patient) {
            patient.visits = patient.visits
                .filter(v =>  v.toString() != visit_id.toString());
        
            await patient.save();
        }
        return patient;
    }

    /**
     * Find patient by his user id
     * @param user_id used id used for quering patient
     * @returns found patient
     */
    async findByUserId(user_id: string) {
        return await this.findOneBy({user_id: user_id});
    }
}

