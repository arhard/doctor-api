import { Visit, IVisit } from '../models/visit.model';
import { AbstractRepository } from './abstract.repository';



export class VisitRepository extends AbstractRepository<IVisit, string> {
    /**
     * Singleton instance
     */
    private static instance = new VisitRepository(Visit, ['schedule_id', 'patient_id']);
    
    /**
     * Returns instance
     * @return singleton instance
     */
    static getInstance() {
        return VisitRepository.instance;
    }

    /**
     * Find a single schedule using it's id
     * @param id schedule's id
     * @returns found schedule
     */
    async findByScheduleId(id: string) {
        return super.findBy({schedule_id: id});
    }

}
