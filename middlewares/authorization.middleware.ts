import { Request, Response, NextFunction } from "express";
import { PatientRepository } from "../repositories/patient.repository";

const patientRepository = PatientRepository.getInstance();



export function checkOwnerPatient(idFieldName: string) {
    return async (req: Request, res: Response, next: NextFunction) => {
        // if admin then owner doesn't matter
        if(req.user.roles.filter(role => role === process.env.ROLE_ADMIN)) {
            next();
        } else {
            // currents user id
            const currentUserId = req.user._id;
            // patients user id
            const patientUserid = (await patientRepository.findById(req.params.id)).user_id._id;

            if(currentUserId.toString() === patientUserid.toString()) {            
                next();
            } else {
                res.status(401).json({message: "Unauthorized"});
                return;
            }
        }
    }
}

// authorized if all roles match
export function checkForRoles(...roles: string[]) {
    // for each user's role
    // check if it's one of the required roles
    // if so then count it as matched
    // if matched count will be equal to the number of user's roles then its true

    return (req: Request, res: Response, next: NextFunction) => {
        let matched = 0;
        // console.log('.Matching for roles');
        req.user.roles.forEach(r => {
            roles.forEach(l => {
                // console.log('.User Role:', r, 'Required role:', l);
                if(r == l) matched++;
            });
        });

        // console.log('Matched:', matched);
        // console.log('roles.length:', roles.length);
        if(matched == roles.length) {
            next();
        } else {
            res.status(401).json({message: "Unauthorized"});
         return;
        }
    }
}

// authorized if at least one role matches
export function checkForAnyRole(...roles: string[]) {
    return (req: Request, res: Response, next: NextFunction) => {
        let matched = 0;
        // console.log('.Matching for any role');
        req.user.roles.forEach(r => {
            roles.forEach(l => {
                // console.log('.User Role:', r, 'Required role:', l);
                if(r == l) matched++;
            });
        });
        if(matched > 0) {
            next();
        } else {
            res.status(401).json({message: "Unauthorized"});
         return;
        }
    }
}